<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Доска доской </title>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=222') }}" >
 </head>
 <body>
  <div class = "wrapper">
   <header class = "header">
	<ul>
	 <li> <a href = "/board/index/"> ГЛАВНАЯ СТРАНИЦА </a></li>
	 <li> <a href = "#"> О НАС </a></li>
	 <li> <a href = "#"> КОНТАКТЫ </a></li>
	 @guest
	 <li><a href = "{{ route('login') }}"> АДМИН </a></li>
	 @else
	 <li>
	 <a href="{{ route('logout') }}"
	 onclick="event.preventDefault();
	 document.getElementById('logout-form').submit();">
	 ВЫЙТИ
	</a>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	@csrf
	</form>
	</li>
	 @endguest
</ul>
   </header>
   <section class = "center">
    <div class = "roster">
	 <form action="" method="GET">
	  <select name="listBoards">
       <option disabled selected>Выберите категорию объявлений...</option>
@for ($i = 1; $i < 6; $i++)
	 <option value= {{ $i }}> {{ $categories[$i] }}</option>
@endfor
      </select>
	  <input type="submit" value="Найти!">
	 </form> 
	 <br>
	 @if($value)
	  <h3> Раздел: {{ $section->title }} </h3>
	 @endif
	 </div><br><br>
   </section>	 
@foreach($sec as $elem)
@if($elem->status > 0)
<p> Автор объявления и информация для связи: {{$elem->contacts }} </p>
<p> Текст объявления:{{$elem->text }} </p>
<p> Дата подачи объявления:{{$elem->time }} </p> 
<br><br>
@endif	
@endforeach

{{ $sec->links() }}

@if($value)
<br><br>
<div class="cent">
	<form action="" method="POST">
	 {{ csrf_field() }}
	 <input name="section" type="hidden" value="{{ Session::get('listBoards') }}">
	 <input name="contacts" class="in_form" type="text" placeholder="Ваши контакты">
	 <br><br>
	 <textarea name="text">Текст Вашего сообщения</textarea>
	 <br><br>
	 <input type="submit" name="public" value="Опубликовать!">
	</form>
</div>
@endif

@if(Session::has('succes'))
<br><p class="succes">{{Session::pull('succes')}}<a href="#"> О НАС </a></p>
@endif
   <footer>
    <p><img src =" {{ asset('storage/company.png') }} " alt = "company"></p>
	<p>Copyright © 2001 - 2021  Board.yes</p>
   </footer>
   </div>
 </body>   
</html>  