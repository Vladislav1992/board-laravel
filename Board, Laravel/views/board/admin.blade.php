<!DOCTYPE html>
<html lang="ru">
 <head>
  <meta charset = "utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Доска доской </title>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >
 </head>
 <body>
  <div class = "wrapper">
   <header class = "header">
	<ul>
	 <li> <a href = "/board/"> ГЛАВНАЯ СТРАНИЦА </a></li>
	 <li> <a href = "#"> О НАС </a></li>
	 <li> <a href = "#"> КОНТАКТЫ </a></li>
	 @guest
	 <li><a href = "{{ route('login') }}"> АДМИН </a></li>
	 @else
	 <li>
	 <a href="{{ route('logout') }}"
	 onclick="event.preventDefault();
	 document.getElementById('logout-form').submit();">
	 ВЫЙТИ
	</a>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	@csrf
	</form>
	</li>
	 @endguest
</ul>
   </header> 
   <table border="1">
    <tr>
	 <td>Название секции</td>
	 <td>id объявления</td>
	 <td>id секции</td>
	 <td>контакты автора</td>
	 <td>текст автора</td>
	 <td>время публикации</td>
	 <td>удалить</td>
	</tr> 
@foreach($sec as $elem)
    <tr>
<td>{{$elem->section->title }}</td>
<td>{{ $elem->id }}</td>
<td>{{$elem->section_id }}</td>
<td>{{$elem->contacts }}</td> 
<td>{{$elem->text }}</td> 
<td>{{$elem->time }}</td>
<td><a href="{{route('board.admin', ['del' => $elem->id])}}">удалить</a></td>
    </tr>
@endforeach
   </table>
{{ $sec->links() }}
   <footer>
    <p><img src =" {{ asset('storage/company.png') }} " alt = "company"></p>
	<p>Copyright © 2001 - 2021  Board.yes</p>
   </footer>
   </div>
 </body>   
</html>  