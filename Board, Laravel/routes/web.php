<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::match(['get', 'post'], '/board/', 'Board\MainController@show')->name('index');
Route::get('/board/index/', function(){
	session()->flush();
	return redirect(route('index'));
});
//Route::get('/board/admin', //'Board\AdminController@admin')->name('board.admin')->middleware('auth');
Route::match(['get', 'post'], '/board/admin/', 'Board\AdminController@admin')->name('board.admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
