<?php
namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controllers;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
	protected $table = 'section';
	public $timestamps = false;
	
	public function board()
	{
		return $this->hasMany('App\Http\Controllers\Board\Board', 'section_id');
	}
	
}