<?php
namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Board\Board;
use App\Http\Controllers\Board\Section;
use Illuminate\Support\Facades\URL;

class AdminController extends Controller
{
	public function admin(Request $request)
	{
		$sec = Board::where('id', '>', 0)->paginate(6);
		
		if($request->has('del')){
			$del = Board::where('id', $request->del)->delete();
		}
			
		return view('board.admin', ['sec' => $sec]);
	}
}