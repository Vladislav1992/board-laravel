<?php
namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controllers;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
	protected $table = 'board';
	public $timestamps = false;
	
	public function section()
	{
		return $this->belongsTo('App\Http\Controllers\Board\Section', 'section_id');
	}
	
}