<?php
namespace App\Http\Controllers\Board;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Board\Board;
use App\Http\Controllers\Board\Section;

class MainController extends Controller
{
	public function show(Request $request)
	{
		if($request->listBoards){
			$request->session()->put('listBoards', $request->listBoards);
		}
		
		$section = '';
		if($request->session()->has('listBoards')){
			$listBoards = $request->session()->get('listBoards');
		$sec = Board::where('section_id', $listBoards)->where('status', '>', 0)->paginate(2);
		} else {
		$date = date('Y:m:d', strtotime("-3 day"));
		$sec = Board::where('time', '>', $date)->where('status', '>', 0)->paginate(2);
		}
		
		if($request->has('public')){
			if(!empty($request->section) && !empty($request->contacts) && !empty($request->text)){
				$board = new Board;
				$board->section_id = $request->section;
				$board->contacts = $request->contacts;
				$board->text = $request->text;
				$board->status = 0;
				$board->time = date('Y:m:d' , strtotime("now"));
				$board->save();
				
				$success = "Ваше объявление сохранено! Пожалуйста, 
				дождитесь его публикации. Если оно не будет опубликовано в 
				течение суток, значит оно было с нарушениями и Вам нужно
				создать новое. Подробнее о правилах публикации можно почитать
				 в разделе ";
				$request->session()->flash('succes', $success);
			}
		}		
		
		$value = $request->fullUrl();
		$value1 = '';
		
		if($request->session()->has('listBoards')){
			$value1 = true;
			$section = Section::find($request->session()->get('listBoards'));
		}
		
		return view('board.main', ['sec' => $sec, 'categories' => [1 => 'Авто', 2 => 'Недвижимость', 3 => 'Книги и печатная продукция', 4 => 'Сад, дача, огород', 5 => 'Эскорт услуги'], 'value' => $value1, 'section' => $section]);
	}
}